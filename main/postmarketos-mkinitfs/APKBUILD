# Maintainer: Oliver Smith <ollieparanoid@postmarketos.org>
# Co-Maintainer: Clayton Craft <clayton@craftyguy.net>
pkgname=postmarketos-mkinitfs
pkgver=1.1.1
pkgrel=1
pkgdesc="Tool to generate initramfs images for postmarketOS"
url="https://postmarketos.org"
depends="
	boot-deploy
	busybox-extras
	bzip2
	cryptsetup
	device-mapper
	e2fsprogs
	e2fsprogs-extra
	f2fs-tools
	lz4
	multipath-tools
	parted
	postmarketos-fde-unlocker
	xz
	"
makedepends="go"
replaces="mkinitfs"
triggers="$pkgname.trigger=/etc/postmarketos-mkinitfs/hooks:/usr/share/kernel/*:/usr/share/postmarketos-mkinitfs-triggers"
source="
	https://gitlab.com/postmarketOS/postmarketos-mkinitfs/-/archive/$pkgver/postmarketos-mkinitfs-$pkgver.tar.gz
	00-default.modules
	init.sh
	init_functions.sh
	"
install="$pkgname.post-upgrade"
arch="all"
license="GPL-2.0-or-later"
provides="mkinitfs=0.0.1"

export GOPATH="$srcdir"
export CGO_ENABLED=0

build() {
	# "-s -w" build a stripped binary
	go build -v -ldflags="-s -w"
}

package() {
	install -Dm644 "$srcdir/init_functions.sh" \
		"$pkgdir/usr/share/postmarketos-mkinitfs/init_functions.sh"

	install -Dm755 "$srcdir/init.sh" \
		"$pkgdir/usr/share/postmarketos-mkinitfs/init.sh"

	install -Dm644 "$srcdir/00-default.modules" \
		"$pkgdir/etc/postmarketos-mkinitfs/modules/00-default.modules"

	install -Dm755 postmarketos-mkinitfs \
		"$pkgdir/sbin/postmarketos-mkinitfs"

	ln -s /sbin/postmarketos-mkinitfs \
		"$pkgdir/sbin/mkinitfs"

	mkdir -p "$pkgdir/etc/postmarketos-mkinitfs/hooks/"
}

check() {
	go test ./...
}

sha512sums="
dd7ec89b766e2cac17b9174872e1359bd3e67d11bd0aeb946ad57feb711d051396777491c2d0783799d4a663043a0094851d5e45e99747272b4fd859d146a83b  postmarketos-mkinitfs-1.1.1.tar.gz
950ac042f19055979cb53b39be93866c88aba0acd5a49cd768522505991e2bd2851735677e777caa6c8973e006318582ddd975214eccc5c35c2c1d649af6d71e  00-default.modules
40033b421e79999c85bd72e1353fe6745a87fcbf9f6a5b8180e832c7f340da7d4e33d056f557ae95a9924f5d186a6e728f3ed53c0922cdac4c39513fdc3e3a82  init.sh
2a8c4d11df8dfbda0b3c87cbb4a186c3686730ed28beb5f7c5b7527080bf4470e678404573d66a3ccbdbc969b3179be54b4535cfd16b6161c8ca214fe53142ee  init_functions.sh
"
